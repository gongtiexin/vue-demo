intelligence-system-frontend


### 下载代码及安装依赖包（需要Node >= 6）
``` 
npm install -g rimraf cross-env webpack webpack-dev-server
mkdir -p ~/hldata/intelligence-system-frontend
cd ~/hldata/intelligence-system-frontend
npm install
```

如果解析不了.vue格式的文件,请在plugin里面下一个vue.js插件

### 浏览器渲染(8010端口)
```
npm run dev
npm run build//打包
```

### 服务器渲染(8080端口)
```
npm run ssr-dev
npm run ssr-build//打包
```

### nginx 配置
新建`intelligence.test.conf`文件到 `$NGINX_HOME/conf.d` 目录:
```
server {
    listen       80;
    server_name  intelligence.test;

    charset utf-8;

    location / {
        root   /home/hldev/hldata/intelligence-system-frontend/dist;
        try_files $uri /index.html =404;
    }

    location /agent {
        proxy_pass http://localhost:29999;
        proxy_redirect off;
    }

    location /api {
        proxy_pass http://localhost:29999;
        proxy_redirect off;
    }

    location /sign {
        proxy_pass http://localhost:29999;
        proxy_redirect off;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

}
*（注意，修改`/home/hldev/hldata`为实际用户或项目所在目录，修改`proxy_pass`为实际代理地址）*


修改 `/etc/hosts` 文件，添加:

```
127.0.0.1       intelligence.test
打开浏览器访问: [http://intelligence.test/](http://intelligence.test/)
