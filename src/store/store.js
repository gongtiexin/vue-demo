/**
 * Created by hldev on 17-3-13.
 */
import Vue from 'vue';
import Vuex from 'vuex';
import news from './modules/news';
import user from './modules/user';
import nlp from './modules/nlp';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        news,
        user,
        nlp,
    },
})