/**
 * Created by hldev on 17-4-27.
 */
import setting from '../../config/setting';
import store from '../store'

export default {
    authenticated(){
        return store.state.user.user.data ? true : false
    },

    logout(){
        store.dispatch('logout')
    },
}