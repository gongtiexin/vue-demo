/**
 * Created by hldev on 17-3-13.
 */
import {newsTYPE} from "../types";
import {request} from "../services/axios";

const state = {
    newsList: []
};

const mutations = {
    [newsTYPE.NEWS_LIST] (state, payload) {
        state.newsList = payload;
    }
};

const actions = {
    findNewsPage({commit}, params) {
        return request(
            {method: 'get', url: '/agent/news/list', params: params},
            {type:newsTYPE.NEWS_LIST},
            {type:newsTYPE.NEWS_LIST_ERROR,message:'搜索失败'},
            commit)
    }
};

export default {
    state,
    mutations,
    actions,
}