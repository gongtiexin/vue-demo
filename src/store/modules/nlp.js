/**
 * Created by hldev on 17-5-3.
 */
import {nlpTYPE} from "../types";
import {request} from "../services/axios";

const state = {
    segment: {},
    analyze: [],
    currentAnalyze: '',

};

const mutations = {
    [nlpTYPE.SEGMENT] (state, payload) {
        state.segment = payload;
    },
    [nlpTYPE.ANALYZE] (state, payload) {
        state.analyze = payload;
    },
    [nlpTYPE.CURRENT_ANALYZE] (state, payload) {
        state.currentAnalyze = payload;
    }
};

const getters = {
    refactorAnalyze: state => {
        const typesMap = [
            {label: '标准分词', value: 'stardard'},
            {label: 'NLP', value: 'nlp'},
            {label: '索引分词', value: 'index'},
            {label: 'N-最短路径分词', value: 'nshort'},
            {label: '最短路径分词', value: 'shortest'},
            {label: 'CRF分词', value: 'crf'},
            {label: '极速分词', value: 'speed'}];

        return state.analyze.items ? state.analyze.items.filter(analyze => (
            analyze._algorithm = typesMap.find(obj => obj.value === analyze.algorithm).label,
                analyze._classifications = analyze.classifications[0].items[0] ? analyze.classifications[0].items[0].category : '暂无此类标签',
                analyze._emotions = analyze.emotions[0].item.result,
                analyze._sensitives = analyze.sensitives.length === 0 ? '否' : '是')
        ) : []
    }
};

const actions = {
    segment({commit}, params) {
        return request(
            {
                method: 'post',
                url: `/agent/nlp/segment`,
                headers: {'Content-Type': 'text/plain'},
                params: {type: params.type},
                data: params.data
            },
            {type: nlpTYPE.SEGMENT},
            {type: nlpTYPE.SEGMENT_ERROR, message: '分析失败'},
            commit)
    },
    analyze({commit}, data) {
        return request(
            {
                method: 'post',
                url: `/agent/nlp/analyze`,
                data
            },
            {type: nlpTYPE.ANALYZE},
            {type: nlpTYPE.ANALYZE_ERROR, message: '分析失败'},
            commit)
    },
    currentAnalyze({commit}, type){
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                commit(nlpTYPE.CURRENT_ANALYZE, type);
                resolve()
            }, 1000)
        })
    },
    clear({commit}){
        commit(nlpTYPE.ANALYZE, [])
    }
};

export default {
    state,
    mutations,
    actions,
    getters,
}