/**
 * Created by hldev on 17-4-27.
 */
import {userTYPE} from "../types";
import {request} from "../services/axios";

const state = {
    user: {}
};

const mutations = {
    [userTYPE.USER_LOGIN] (state, payload) {
        state.user = payload;
    }
};

const actions = {
    login({commit}, data) {
        return request(
            {method: 'post', url: '/sign/login', data},
            {type: userTYPE.USER_LOGIN, message: '登录成功'},
            {type: userTYPE.USER_LOGIN_ERROR, message: '登录失败'},
            commit)
    },
    logout({commit}) {
        return commit(userTYPE.USER_LOGIN, {})
    },
    register({commit}, data) {
        return request(
            {method: 'post', url: '/sign/register', data},
            {type: userTYPE.USER_REGISTER},
            {type: userTYPE.USER_REGISTER_ERROR, message: '注册失败'},
            commit)
    },
    getCurrentUser({commit}) {
        return request(
            {method: 'get', url: '/api/user'},
            {type: userTYPE.USER_LOGIN},
            {type: userTYPE.USER_LOGIN_ERROR},
            commit)
    }
};

export default {
    state,
    mutations,
    actions,
}