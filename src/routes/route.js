/**
 * Created by hldev on 17-3-8.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import ItemList from '../components/news/ItemList.vue'
import NewsDetail from '../components/news/NewsDetail.vue'
import Login from '../components/user/Login.vue'
import Register from '../components/user/Register.vue'
import Nlp from '../components/Nlp.vue'

Vue.use(VueRouter);

const routes = [
    /*首页*/
    {
        path: "/",
        component: ItemList,
        redirect: '/new'
    },
    /*自然语言分析*/
    {
        path: "/nlp",
        component: Nlp,
    },
    /*用户*/
    {
        path: "/login",
        component: Login,
    }, {
        path: "/register",
        component: Register,
    },
    /*新闻*/
    {
        path: '/new',
        component: ItemList,
        // meta: {requiresAuth: true},
    }, {
        path: '/new/:id',
        component: NewsDetail,
    }
];

const router = new VueRouter({
    mode: 'history',
    routes,
});

import Auth from '../store/services/auth'
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth) && !Auth.authenticated()) {
        next({
            path: '/login',
            querry: {redirect: to.fullPath}
        })
    }
    else {
        next()
    }
})
;

export default router;