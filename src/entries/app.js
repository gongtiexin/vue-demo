/**
 * Created by hldev on 17-4-28.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import router from '../routes/route'
import store from '../store/store'
import home from '../components/home/Home.vue'
import {sync} from 'vuex-router-sync'

Vue.use(ElementUI);
Vue.use(VueRouter);
sync(store, router);

Vue.config.errorHandler = function (err, vm) {
    var componentName = Vue.util.formatComponentName(vm),
        propsData = vm.$options.propsData;
    fundebug.notifyError(err, {
        metaData: {
            componentName: componentName,
            propsData: propsData
        }
    });
};

const app = new Vue({
    el: '#app',
    data: {
        vueImpl: ''
    },
    router,
    store,
    beforeCreate(){
        store.dispatch('getCurrentUser');
    },
    render: h => h(home)
});